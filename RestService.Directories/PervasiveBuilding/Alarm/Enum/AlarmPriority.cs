﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestService.Directories.PervasiveBuilding.Alarm.Enum
{
   public enum AlarmPriority
   {
      High = 100,
      Medium = 50,
      Low = 10,
   }
}
