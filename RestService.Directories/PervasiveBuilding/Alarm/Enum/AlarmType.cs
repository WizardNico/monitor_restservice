﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestService.Directories.PervasiveBuilding.Alarm.Enum
{
   public enum AlarmType
   {
      Fire = 10,
      Intrusion = 20,
   }
}
