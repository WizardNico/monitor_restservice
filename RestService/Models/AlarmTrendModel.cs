﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Models
{
   public class AlarmTrendModel
   {
      public Dictionary<long, int> Fire { get; set; }

      public Dictionary<long, int> Intrusion { get; set; }
   }
}
