﻿using RestService.Directories.PervasiveBuilding.Alarm.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Models
{
    public class Alarm
    {
      public long Id { get; set; }

      public AlarmPriority Priority { get; set; }

      public AlarmType Type { get; set; }

      public DateTime Date { get; set; }

      public long RoomId { get; set; }
   }
}
