﻿using System;

namespace RestService.Models
{
   public class Temperature
   {
      public long Id { get; set; }

      public long RoomId { get; set; }

      public DateTime InsertionDate { get; set; }

      public double TemperatureValue { get; set; }

      public double HumidityValue { get; set; }
   }
}
