﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Models
{
    public class TemperatureVariationModel
    {
      public Dictionary<long, double> Max { get; set; }

      public Dictionary<long, double> Min { get; set; }
   }
}
