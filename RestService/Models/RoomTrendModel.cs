﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Models
{
   public class RoomTrendModel
   {
      [BindProperty]
      public DateTime? Date { get; set; }

      [BindProperty]
      public int? FloorNumber { get; set; }

      [BindProperty]
      public int? RoomNumber { get; set; }
   }
}
