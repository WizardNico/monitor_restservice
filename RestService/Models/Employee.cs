﻿using RestService.Directories.PervasiveBuilding.Employee.Enum;

namespace RestService.Models
{
    public class Employee
    {
      public long Id { get; set; }

      public EmployeeRole Role { get; set; }

      public string Name { get; set; }

      public string Surname { get; set; }

      public string Email { get; set; }

      public double PreferredTemperature { get; set; }

      public long RoomId { get; set; }
   }
}
