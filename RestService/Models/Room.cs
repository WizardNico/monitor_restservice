﻿using System;
using System.Collections.Generic;

namespace RestService.Models
{
   public class Room
   {
      public long Id { get; set; }

      public int RoomNumber { get; set; }

      public int FloorNumber { get; set; }

      public long FireEscapeId { get; set; }

      public FireEscape FireEscape { get; set; }
   }
}
