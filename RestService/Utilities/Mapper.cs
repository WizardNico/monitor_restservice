﻿using RestService.Directories.PervasiveBuilding.Employee.Enum;
using RestService.Models;
using PervasiveRoomModels = RestService.Directories.PervasiveBuilding.Room.Models;
using PervasiveEmployeeModels = RestService.Directories.PervasiveBuilding.Employee.Models;
using PervasiveTemperatureModels = RestService.Directories.PervasiveBuilding.Temperature.Models;
using PervasiveAlarmModels = RestService.Directories.PervasiveBuilding.Alarm.Models;
using RestService.Directories.PervasiveBuilding.Alarm.Enum;

namespace RestService.Utilities
{
   public class Mapper
   {
      public static Temperature Map(PervasiveTemperatureModels.Temperature temperature) => new Temperature()
      {
         Id = temperature.Id.Value,
         InsertionDate = temperature.InsertionDate.Value,
         TemperatureValue = temperature.TemperatureValue.Value,
         HumidityValue = temperature.HumidityValue.Value,
         RoomId = temperature.RoomId.Value,
      };

      public static Room Map(PervasiveRoomModels.Room room) => new Room()
      {
         Id = room.Id.Value,
         FloorNumber = room.FloorNumber.Value,
         RoomNumber = room.RoomNumber.Value,
         FireEscape = new FireEscape() { Code = room.FireEscape.Code, Location = room.FireEscape.Location }
      };

      public static Employee Map(PervasiveEmployeeModels.Employee employee) => new Employee()
      {
         Id = employee.Id.Value,
         Name = employee.Name,
         Surname = employee.Surname,
         Email = employee.Email,
         PreferredTemperature = employee.PreferredTemperature.Value,
         Role = (EmployeeRole)employee.Role,
         RoomId = employee.RoomId.Value,
      };

      public static Alarm Map(PervasiveAlarmModels.Alarm alarm) => new Alarm()
      {
         Id = alarm.Id.Value,
         Date = alarm.Date.Value,
         Priority = (AlarmPriority)alarm.Priority,
         Type = (AlarmType)alarm.Type,
         RoomId = alarm.RoomId.Value,
      };
   }
}
