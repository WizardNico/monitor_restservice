﻿using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Directories.PervasiveBuilding.Alarm.Enum;
using RestService.Models;
using PervasiveRoomModels = RestService.Directories.PervasiveBuilding.Room.Models;
using PervasiveAlarmModels = RestService.Directories.PervasiveBuilding.Alarm.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Controllers
{
   public class AlarmTrendController : Controller
   {
      private IAgileBus bus;

      public AlarmTrendController(IAgileBus bus)
      {
         this.bus = bus;
      }

      public IActionResult Index()
      {
         return View();
      }

      public IActionResult About()
      {
         ViewData["Message"] = "Your application description page.";

         return View();
      }

      public IActionResult Contact()
      {
         ViewData["Message"] = "Your contact page.";

         return View();
      }

      public IActionResult Error()
      {
         return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
      }

      public async Task<ActionResult> AlarmTrendAsync()
      {
         //Get all rooms
         Dictionary<string, object> roomFilter = new Dictionary<string, object>
         {
            { "floorNumber", null },
            { "roomNumber", null },
         };
         List<PervasiveRoomModels.Room> roomList = await bus.GetListAsync<PervasiveRoomModels.Room>(roomFilter);

         //Get all alarms
         Dictionary<string, object> alarmFilter = new Dictionary<string, object>
         {
            { "alarmType", null },
            { "roomId", null },
         };
         List<PervasiveAlarmModels.Alarm> alarmList = await bus.GetListAsync<PervasiveAlarmModels.Alarm>(alarmFilter);

         var model = GetDataAssets(GetAlarmTrendModel(alarmList, roomList));

         return View(model);
      }

      private string[][] GetDataAssets(AlarmTrendModel Model)
      {
         List<string[]> data = new List<string[]>();

         foreach (var kvpair in Model.Fire)
         {
            data.Add(new[] { kvpair.Key.ToString(), Model.Intrusion[kvpair.Key].ToString(), kvpair.Value.ToString() });
         }

         return data.ToArray();
      }

      private AlarmTrendModel GetAlarmTrendModel(List<PervasiveAlarmModels.Alarm> AlarmList, List<PervasiveRoomModels.Room> RoomList)
      {
         //Building models adding room and then countign events on it
         var intrusionModel = new Dictionary<long, int>();
         var fireModel = new Dictionary<long, int>();

         foreach (PervasiveRoomModels.Room room in RoomList)
         {
            intrusionModel.Add(room.Id.Value, 0);
            fireModel.Add(room.Id.Value, 0);

            //Count events on this room
            foreach (PervasiveAlarmModels.Alarm alarm in AlarmList.Where(a => a.RoomId == room.Id.Value).ToList())
            {
               switch (alarm.Type)
               {
                  case AlarmType.Fire:
                     //Update value
                     fireModel[room.Id.Value] = fireModel[room.Id.Value] + 1;
                     break;

                  case AlarmType.Intrusion:
                     //Update value
                     intrusionModel[room.Id.Value] = intrusionModel[room.Id.Value] + 1;
                     break;
               }
            }
         }

         return new AlarmTrendModel()
         {
            Fire = fireModel,
            Intrusion = intrusionModel,
         };
      }
   }
}
