﻿using AgileMQ.DTO;
using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Models;
using RestService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PervasiveTemperatureModels = RestService.Directories.PervasiveBuilding.Temperature.Models;

namespace RestService.Controllers
{
   [Route("api/[controller]")]
   public class TemperatureController : Controller
   {
      private IAgileBus bus;

      public TemperatureController(IAgileBus bus)
      {
         this.bus = bus;
      }

      [HttpGet("{id}")]
      public async Task<IActionResult> Get(long id)
      {
         PervasiveTemperatureModels.Temperature temperature = new PervasiveTemperatureModels.Temperature()
         {
            Id = id
         };
         temperature = await bus.GetAsync(temperature);
         Temperature result = Mapper.Map(temperature);

         return Ok(result);
      }

      [HttpGet]
      public async Task<IActionResult> GetList(long? roomId)
      {
         Dictionary<string, object> filter = new Dictionary<string, object>
         {
            { "roomId", roomId },
         };

         List<PervasiveTemperatureModels.Temperature> list = await bus.GetListAsync<PervasiveTemperatureModels.Temperature>(filter);

         List<Temperature> items = new List<Temperature>();
         foreach (PervasiveTemperatureModels.Temperature temperature in list)
         {
            items.Add(Mapper.Map(temperature));
         }

         return Ok(items);
      }


      [HttpPost]
      public async Task<IActionResult> Post([FromBody]Temperature temperature)
      {
         PervasiveTemperatureModels.Temperature msgTemperature = new PervasiveTemperatureModels.Temperature()
         {
            TemperatureValue = temperature.TemperatureValue,
            HumidityValue = temperature.HumidityValue,
            RoomId = temperature.RoomId,
         };

         msgTemperature = await bus.PostAsync(msgTemperature);

         return Ok(msgTemperature.Id);
      }

      [HttpDelete]
      public async Task<IActionResult> Delete(long id)
      {
         PervasiveTemperatureModels.Temperature temperature = new PervasiveTemperatureModels.Temperature()
         {
            Id = id
         };

         await bus.DeleteAsync(temperature);
         return Ok();
      }
   }
}
