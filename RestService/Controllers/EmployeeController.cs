﻿using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using PervasiveEmployeeModels = RestService.Directories.PervasiveBuilding.Employee.Models;
using System.Threading.Tasks;
using RestService.Utilities;
using RestService.Models;
using System.Collections.Generic;
using RestService.Directories.PervasiveBuilding.Employee.Enum;

namespace RestService.Controllers
{
   [Route("api/[controller]")]
   public class EmployeeController : Controller
   {
      private IAgileBus bus;

      public EmployeeController(IAgileBus bus)
      {
         this.bus = bus;
      }

      [HttpGet("{id}")]
      public async Task<IActionResult> Get(long id)
      {
         PervasiveEmployeeModels.Employee employee = new PervasiveEmployeeModels.Employee()
         {
            Id = id
         };
         employee = await bus.GetAsync(employee);
         Employee result = Mapper.Map(employee);

         return Ok(result);
      }

      [HttpGet]
      public async Task<IActionResult> GetList(double? preference, EmployeeRole? role, long? roomId)
      {
         Dictionary<string, object> filter = new Dictionary<string, object>
         {
            { "roomId", roomId },
            { "preference", preference },
            { "role", role },
         };

         List<PervasiveEmployeeModels.Employee> list = await bus.GetListAsync<PervasiveEmployeeModels.Employee>(filter);

         List<Employee> items = new List<Employee>();
         foreach (PervasiveEmployeeModels.Employee employee in list)
         {
            items.Add(Mapper.Map(employee));
         }

         return Ok(items);
      }

      [HttpPost]
      public async Task<IActionResult> Post([FromBody]Employee employee)
      {
         PervasiveEmployeeModels.Employee msgEmployee = new PervasiveEmployeeModels.Employee()
         {
            Name = employee.Name,
            Surname = employee.Surname,
            Email = employee.Email,
            PreferredTemperature = employee.PreferredTemperature,
            RoomId = employee.RoomId,
            Role = employee.Role
         };

         msgEmployee = await bus.PostAsync(msgEmployee);

         return Ok(msgEmployee.Id);
      }

      [HttpPut]
      public async Task<IActionResult> Put([FromBody]Employee employee)
      {
         PervasiveEmployeeModels.Employee msgEmployee = new PervasiveEmployeeModels.Employee()
         {
            Id = employee.Id,
            Name = employee.Name,
            Surname = employee.Surname,
            Email = employee.Email,
            PreferredTemperature = employee.PreferredTemperature,
            RoomId = employee.RoomId,
            Role = employee.Role
         };

         await bus.PutAsync(msgEmployee);

         return Ok();
      }

      [HttpDelete]
      public async Task<IActionResult> Delete(long id)
      {
         PervasiveEmployeeModels.Employee msgEmployee = new PervasiveEmployeeModels.Employee()
         {
            Id = id
         };

         await bus.DeleteAsync(msgEmployee);
         return Ok();
      }
   }
}
