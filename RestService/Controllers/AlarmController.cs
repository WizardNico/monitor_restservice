﻿using Microsoft.AspNetCore.Mvc;
using PervasiveAlarmModels = RestService.Directories.PervasiveBuilding.Alarm.Models;
using System.Threading.Tasks;
using RestService.Directories.PervasiveBuilding.Alarm.Enum;
using AgileMQ.Interfaces;
using RestService.Models;
using System.Collections.Generic;
using RestService.Utilities;

namespace RestService.Controllers
{
   [Route("api/[controller]")]
   public class AlarmController : Controller
   {
      private IAgileBus bus;

      public AlarmController(IAgileBus bus)
      {
         this.bus = bus;
      }

      [HttpGet("{id}")]
      public async Task<IActionResult> Get(long id)
      {
         PervasiveAlarmModels.Alarm alarm = new PervasiveAlarmModels.Alarm()
         {
            Id = id
         };
         alarm = await bus.GetAsync(alarm);
         Alarm result = Mapper.Map(alarm);

         return Ok(result);
      }

      [HttpGet]
      public async Task<IActionResult> GetList(AlarmType? type, long? roomId)
      {
         Dictionary<string, object> filter = new Dictionary<string, object>
         {
            { "alarmType", type },
            { "roomId", roomId },
         };

         List<PervasiveAlarmModels.Alarm> list = await bus.GetListAsync<PervasiveAlarmModels.Alarm>(filter);

         List<Alarm> items = new List<Alarm>();
         foreach (PervasiveAlarmModels.Alarm alarm in list)
         {
            items.Add(Mapper.Map(alarm));
         }

         return Ok(items);
      }

      [HttpPost]
      public async Task<IActionResult> Post([FromBody]Alarm alarm)
      {
         PervasiveAlarmModels.Alarm mgsAlarm = new PervasiveAlarmModels.Alarm()
         {
            Priority = alarm.Priority,
            Type = alarm.Type,
            RoomId = alarm.RoomId,
         };

         if (alarm.Type == AlarmType.Fire)
         {
            await bus.NotifyAsync(mgsAlarm, "FireDetected");
         }
         else
         {
            await bus.NotifyAsync(mgsAlarm, "IntrusionDetected");
         }

         return Ok();
      }
   }
}
