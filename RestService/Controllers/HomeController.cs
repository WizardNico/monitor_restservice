﻿using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Models;
using System.Diagnostics;

namespace RestService.Controllers
{
   public class HomeController : Controller
   {
      private IAgileBus bus;

      public HomeController(IAgileBus bus)
      {
         this.bus = bus;
      }

      public IActionResult Index()
      {
         return View();
      }

      public IActionResult About()
      {
         ViewData["Message"] = "Your application description page.";

         return View();
      }

      public IActionResult Contact()
      {
         ViewData["Message"] = "Your contact page.";

         return View();
      }

      public IActionResult Error()
      {
         return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
      }

      public ActionResult RoomTrend() { return View(); }
   }
}
