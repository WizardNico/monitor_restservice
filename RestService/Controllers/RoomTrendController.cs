﻿using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using RestService.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using PervasiveRoomModels = RestService.Directories.PervasiveBuilding.Room.Models;
using PervasiveTemperatureModels = RestService.Directories.PervasiveBuilding.Temperature.Models;

namespace RestService.Controllers
{
   public class RoomTrendController : Controller
   {
      private IAgileBus bus;

      public RoomTrendController(IAgileBus bus)
      {
         this.bus = bus;
      }

      public IActionResult Index()
      {
         return View();
      }

      public IActionResult About()
      {
         ViewData["Message"] = "Your application description page.";

         return View();
      }

      public IActionResult Contact()
      {
         ViewData["Message"] = "Your contact page.";

         return View();
      }

      public IActionResult Error()
      {
         return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
      }

      public ActionResult RoomSelector()
      {
         if (TempData["ERROR"] != null)
         {
            ModelState.AddModelError("ERROR", TempData["ERROR"].ToString());
         }

         if (!ModelState.IsValid)
         {
            return View(ModelState);
         }
         else
         {
            return View();
         }
      }

      public async Task<ActionResult> RoomTrendAsync(RoomTrendModel Model)
      {
         //Checking if the user insert all fields
         if (Model.Date == null)
         {
            TempData["ERROR"] = "Please Select a Date";
            return RedirectToAction("RoomSelector", "RoomTrend");
         }

         if (Model.RoomNumber == null)
         {
            TempData["ERROR"] = "Please Select a Room Number";
            return RedirectToAction("RoomSelector", "RoomTrend");
         }

         if (Model.FloorNumber == null)
         {
            TempData["ERROR"] = "Please Select a Floor Number";
            return RedirectToAction("RoomSelector", "RoomTrend");
         }

         //Getting the selected room
         Dictionary<string, object> roomFilter = new Dictionary<string, object>
         {
            { "floorNumber", Model.FloorNumber },
            { "roomNumber", Model.RoomNumber },
         };
         List<PervasiveRoomModels.Room> RoomList = await bus.GetListAsync<PervasiveRoomModels.Room>(roomFilter);

         //Check if the room exist or not
         if (RoomList.Count == 0)
         {
            TempData["ERROR"] = "Non-Existent Room";
            return RedirectToAction("RoomSelector", "RoomTrend");
         }

         //Get all temperature of this room
         Dictionary<string, object> temperatureFilter = new Dictionary<string, object>
         {
            { "roomId", RoomList[0].Id },
         };
         List<PervasiveTemperatureModels.Temperature> temperatureList = await bus.GetListAsync<PervasiveTemperatureModels.Temperature>(temperatureFilter);

         //Filtering temperature getting all temperature of the selected month
         var returnModel = temperatureList.
            Where(tmp => tmp.InsertionDate.Value.Month == Model.Date.Value.Month && tmp.InsertionDate.Value.Year == Model.Date.Value.Year)
            .ToList();

         return View(returnModel);
      }
   }
}
