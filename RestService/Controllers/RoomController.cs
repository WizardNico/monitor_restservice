﻿using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Models;
using System.Collections.Generic;
using PervasiveRoomModels = RestService.Directories.PervasiveBuilding.Room.Models;
using System.Threading.Tasks;
using RestService.Utilities;

namespace RestService.Controllers
{
   [Route("api/[controller]")]
   public class RoomController : Controller
   {
      private IAgileBus bus;

      public RoomController(IAgileBus bus)
      {
         this.bus = bus;
      }

      [HttpGet("{id}")]
      public async Task<IActionResult> Get(long id)
      {
         PervasiveRoomModels.Room room = new PervasiveRoomModels.Room()
         {
            Id = id
         };
         room = await bus.GetAsync(room);
         Room result = Mapper.Map(room);

         return Ok(result);
      }

      [HttpGet]
      public async Task<IActionResult> GetList(int? floorNumber, int? roomNumber)
      {
         Dictionary<string, object> filter = new Dictionary<string, object>
         {
            { "floorNumber", floorNumber },
            { "roomNumber", roomNumber },
         };

         List<PervasiveRoomModels.Room> list = await bus.GetListAsync<PervasiveRoomModels.Room>(filter);

         List<Room> items = new List<Room>();
         foreach (PervasiveRoomModels.Room room in list)
         {
            items.Add(Mapper.Map(room));
         }

         return Ok(items);
      }

      [HttpPost]
      public async Task<IActionResult> Post([FromBody]Room room)
      {
         PervasiveRoomModels.Room msgRoom = new PervasiveRoomModels.Room()
         {
            FloorNumber = room.FloorNumber,
            RoomNumber = room.RoomNumber,
            FireEscapeId = room.FireEscapeId
         };

         msgRoom = await bus.PostAsync(msgRoom);

         return Ok(msgRoom.Id);
      }

      [HttpPut]
      public async Task<IActionResult> Put([FromBody]Room room)
      {
         PervasiveRoomModels.Room msgRoom = new PervasiveRoomModels.Room()
         {
            Id = room.Id,
            FloorNumber = room.FloorNumber,
            RoomNumber = room.RoomNumber,
            FireEscapeId = room.FireEscapeId
         };

         await bus.PutAsync(msgRoom);

         return Ok();
      }

      [HttpDelete]
      public async Task<IActionResult> Delete(long id)
      {
         PervasiveRoomModels.Room msgRoom = new PervasiveRoomModels.Room()
         {
            Id = id
         };

         await bus.DeleteAsync(msgRoom);
         return Ok();
      }
   }
}
