﻿using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Models;
using PervasiveRoomModels = RestService.Directories.PervasiveBuilding.Room.Models;
using PervasiveTemperatureModels = RestService.Directories.PervasiveBuilding.Temperature.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace RestService.Controllers
{
   public class TemperatureVariationController : Controller
   {
      private IAgileBus bus;

      public TemperatureVariationController(IAgileBus bus)
      {
         this.bus = bus;
      }

      public IActionResult Index()
      {
         return View();
      }

      public IActionResult About()
      {
         ViewData["Message"] = "Your application description page.";

         return View();
      }

      public IActionResult Contact()
      {
         ViewData["Message"] = "Your contact page.";

         return View();
      }

      public IActionResult Error()
      {
         return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
      }

      public async Task<ActionResult> TemperatureVariationAsync()
      {
         //Get all rooms
         Dictionary<string, object> roomFilter = new Dictionary<string, object>
         {
            { "floorNumber", null },
            { "roomNumber", null },
         };
         List<PervasiveRoomModels.Room> roomList = await bus.GetListAsync<PervasiveRoomModels.Room>(roomFilter);

         //Get All Temperatures
         Dictionary<string, object> filter = new Dictionary<string, object>
         {
            { "roomId", null },
         };
         List<PervasiveTemperatureModels.Temperature> temperatureList = await bus.GetListAsync<PervasiveTemperatureModels.Temperature>(filter);

         var model = GetDataAssets(GetTemperatureVariationModel(temperatureList, roomList));

         return View(model);
      }

      private string[][] GetDataAssets(TemperatureVariationModel Model)
      {
         List<string[]> data = new List<string[]>();

         foreach (var kvpair in Model.Max)
         {
            data.Add(new[] { kvpair.Key.ToString(), Convert.ToDouble(Model.Min[kvpair.Key]).ToString(System.Globalization.CultureInfo.InvariantCulture), Convert.ToDouble(kvpair.Value).ToString(System.Globalization.CultureInfo.InvariantCulture) });
         }

         return data.ToArray();
      }

      private TemperatureVariationModel GetTemperatureVariationModel(List<PervasiveTemperatureModels.Temperature> TemperatureList, List<PervasiveRoomModels.Room> RoomList)
      {
         //Building models adding room and then countign events on it
         var maxModel = new Dictionary<long, double>();
         var minModel = new Dictionary<long, double>();

         foreach (PervasiveRoomModels.Room room in RoomList)
         {
            //Add MIN
            var minTempList = TemperatureList.Where(
               tmp => tmp.RoomId == room.Id.Value
               && tmp.InsertionDate.Value.Hour == 7 && tmp.InsertionDate.Value.Minute == 0).ToList();

            minModel.Add(room.Id.Value, minTempList.Sum(t => t.TemperatureValue.Value) / minTempList.Count);

            //Add MAX
            var maxTempList = TemperatureList.Where(
               tmp => tmp.RoomId == room.Id.Value
               && tmp.InsertionDate.Value.Hour == 19 && tmp.InsertionDate.Value.Minute == 0).ToList();

            maxModel.Add(room.Id.Value, maxTempList.Sum(t => t.TemperatureValue.Value) / maxTempList.Count);
         }

         return new TemperatureVariationModel()
         {
            Max = maxModel,
            Min = minModel,
         };
      }
   }
}
